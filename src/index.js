import React from "react";
import ReactDOM from "react-dom";
import "./assets/css/index.css";
import App from "./containers/App";

import { Provider } from "react-redux";
import registerServiceWorker from "./registerServiceWorker";
import { history, store } from "./store";
import { ConnectedRouter } from "connected-react-router";

registerServiceWorker();

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
