import React, { Component } from "react";
import { connect } from "react-redux";
import { createGrid } from "../actions/game";
import { bindActionCreators } from "redux";

class ScoreDisplaying extends Component {
  displayEnergy(player, className) {
    if (this.props.game.type !== "dupz") {
      return;
    }
    const array = [1, 2, 3];
    return array.map((x) => {
      if (player.energy >= x) {
        return <div key={x} className={className} />;
      } else {
        return <div key={x} className={"energyEmpty"} />;
      }
    });
  }

  displayTurnPlayer() {
    const { game } = this.props;
    const { players } = game;
    const player1 = players[0];

    if (game.lastPlayerPlayed === undefined) {
      if (player1.id !== game.firstPlayer) {
        return <div className="turnPlayer1" />;
      }
      return <div className="turnPlayer2" />;
    }

    if (game.lastPlayerPlayed === player1.id) {
      return <div className="turnPlayer2" />;
    }
    return <div className="turnPlayer1" />;
  }

  render() {
    if (!this.props.game) return <div />;
    if (!this.props.game.created) return <div />;

    const { players } = this.props.game;
    const player1 = players[0];
    const player2 = players[1];
    return (
      <div className="scoreDisplaying">
        <div className="player1">
          <div className="bloc">
            <div className="player1Color" />
            <div className="usernamePlayer1">{player1.name}</div>
          </div>
          <div className="energy">
            {this.displayEnergy(player1, "energyPlayer1")}
          </div>
        </div>

        <div className="scores">
          <span className="player1Score">{player1.points}</span> -{" "}
          <span className="player2Score">{player2.points}</span>
          {this.props.game.type === "bacteria" ? (
            <div className="playerTurn">{this.displayTurnPlayer()}</div>
          ) : (
            <div />
          )}
        </div>

        <div className="player2">
            <div className="bloc">
              <div className="usernamePlayer2">{player2.name}</div>
              <div className="player2Color" />
            </div>
            <div className="energy energy2">
              {this.displayEnergy(player2, "energyPlayer2")}
            </div>
        </div>
      </div>
    );
  }
}

function mapStateToProp(state) {
  return {
    game: state.game.game,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ createGrid }, dispatch);
}
export default connect(
  mapStateToProp,
  mapDispatchToProps
)(ScoreDisplaying);
