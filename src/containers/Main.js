import React, { Component } from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import LobbyContainer from "./Lobby";
import GameContainer from "./Game";
import { updateNumbers } from "../actions/lobby";
import { retrieveSession } from "../services/SessionService";
import io from "socket.io-client";

import { serverURL } from "../consts";

import "../assets/css/Game.css";

export var socket;

class MainContainer extends Component {
  state = {};

  // - Lifecycle

  componentWillMount() {
    const { history } = this.props;
    if (history.location.pathname === "/game/game") {
      history.push("/game/lobby");
    }
    this.connectSocket();
  }

  connectSocket = () => {
    const session = retrieveSession();
    const { history } = this.props;

    if (session.user.userId === null || session.user.userId === "null") {
      history.push("/login");
    }
    socket = io.connect(
      serverURL,
      {
        query: {
          token: session.token,
        },
        transports: ["websocket"],
      }
    );

    socket.on("tokenExpired", () => {
      alert("Ta session a expiré.");
      history.push("/login");
    });

    socket.on("onGoing", (data) => {
      this.props.updateNumbers(data);
    });
    socket.on("crushingConnection", (data) => {
      alert(
        "Vous vous êtes fait déconnectés car votre compte s'est connecté sur un autre appareil."
      );
      history.push("/login");
    });
  };

  componentDidMount() {
    const { pathname } = this.props.history.location;
    window.onpopstate = () => {
      if (pathname === "/game/lobby") {
        socket.removeAllListeners("crushingConnection");
        socket.removeAllListeners("onGoing");
        socket.removeAllListeners("reconnect");
        socket.removeAllListeners("game_created");
        socket.removeAllListeners("game_paused");
        socket.removeAllListeners("game_unpaused");
        socket.removeAllListeners("update");
        socket.removeAllListeners("cancelMove");
        socket.removeAllListeners("updateEnergy");
        socket.removeAllListeners("ready");
        this.connectSocket();
      }
    };
  }

  // - Rendering
  render() {
    return (
      <Switch>
        <Route path="/game/lobby" component={LobbyContainer} />
        <Route path="/game/game" component={GameContainer} />
      </Switch>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      updateNumbers,
    },
    dispatch
  );
}
export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(MainContainer)
);
