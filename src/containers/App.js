import React, { Component } from "react";
import MetaTags from 'react-meta-tags';
import { Switch, Route, withRouter } from "react-router-dom";

import AuthenticationContainer from "./Authentication";
import MainContainer from "./Main";
import { retrieveSession } from "../services/SessionService";

import "../assets/css/App.css";
import RankingContainer from "./Ranking";

class App extends Component {
  // - Life cycle
  componentWillMount() {
    const session = retrieveSession();
    if (session.token === null || session.token === "null") {
      return this.props.history.push({ pathname: "/login" });
    }
  }
  // - Rendering

  render() {
    return (
      <div className="App">
        <MetaTags>
          </MetaTags>
        <Switch>
          <Route path="/game" component={MainContainer} />
          <Route path="/ranking" component={RankingContainer} />
          <Route path="/" component={AuthenticationContainer} />
        </Switch>
      </div>
    );
  }
}
export default withRouter(App);
