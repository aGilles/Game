import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Card, Typography } from "antd";

import { authenticate } from "../actions/auth";
import AuthForm from "../components/AuthForm";
import "../assets/css/Auth.css";
import Header from "../components/Header/header";

const { Title } = Typography;

class AuthenticationContainer extends Component {
  state = {
    registration: false,
  };

  // - User actions

  async onSubmit(values) {
    const { username, password } = values;
    const { authenticate } = this.props;
    authenticate(username, password, this.state.registration);
  }

  startLogin() {
    this.setState({ registration: false });
  }

  startRegistration() {
    this.setState({ registration: true });
  }

  // - Rendering

  renderErrorLogin() {
    if (this.props.authentication.error) {
      return (
        <div style={{ color: "red", fontSize: 16, fontWeight: "bold" }}>
          {this.props.authentication.error}
        </div>
      );
    }
    return <div />;
  }

  render() {
    return (
      <div className="containerGod">
        <Header />
        <div className="wholeContainer">
          <div className="authContainer">
            <div className="descriptionContainer">
              <div className="description">
                <img
                  src={require("../assets/images/chess-board.png")}
                  width={80}
                  height={80}
                  className="descriptionImg"
                  alt="chess"
                />{" "}
                <span className="context">Jeu de plateau 2.0</span>
              </div>
              <div className="description">
                <img
                  src={require("../assets/images/renewable-energy.png")}
                  width={80}
                  height={80}
                  className="descriptionImg"
                  alt="energy"
                />{" "}
                <span className="context">
                  L'énergie remplace le tour par tour
                </span>
              </div>
              <div className="description">
                <img
                  src={require("../assets/images/duplicate.png")}
                  width={80}
                  height={80}
                  className="descriptionImg"
                  alt="duplicate"
                />{" "}
                <span className="context">Duplique, Saute, Protège, Mange</span>
              </div>
              <div className="description">
                <img
                  src={require("../assets/images/thinking.png")}
                  width={80}
                  height={80}
                  className="descriptionImg"
                  alt="think"
                />
                <span className="context">
                  Stratégie, Réactivité, Dexterité
                </span>
              </div>
            </div>
            <Card bordered={true} className="cardContainerLogin">
              <Title level={2}>Connexion</Title>
              {this.renderErrorLogin()}
              <AuthForm
                isRegistering={this.state.registration}
                startLogin={this.startLogin.bind(this)}
                startRegistration={this.startRegistration.bind(this)}
                onSubmit={this.onSubmit.bind(this)}
              />
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProp(state) {
  return state;
}

export default withRouter(
  connect(
    mapStateToProp,
    { authenticate }
  )(AuthenticationContainer)
);
