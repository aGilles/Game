import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import LobbyComponent from "../components/Lobby";

class LobbyContainer extends Component {
  state = {};

  // - Lifecycle

  // - Rendering

  render() {
    const {
      numberUsers,
      numberGames,
      numberGamesWaitingDupz,
      numberGamesOnGoingDupz,
      numberGamesWaitingFun,
      numberGamesOnGoingFun,
      numberGamesWaitingBacteria,
      numberGamesOnGoingBacteria,
      usernames,
      dupzGames,
      funGames,
      bacteriaGames,
    } = this.props;
    return (
      <LobbyComponent
        usernames={usernames}
        numberUsers={numberUsers}
        numberGames={numberGames}
        numberGamesWaitingDupz={numberGamesWaitingDupz}
        numberGamesOnGoingDupz={numberGamesOnGoingDupz}
        numberGamesWaitingFun={numberGamesWaitingFun}
        numberGamesOnGoingFun={numberGamesOnGoingFun}
        numberGamesWaitingBacteria={numberGamesWaitingBacteria}
        numberGamesOnGoingBacteria={numberGamesOnGoingBacteria}
        dupzGames={dupzGames}
        funGames={funGames}
        bacteriaGames={bacteriaGames}
      />
    );
  }
}
function mapStateToProp(state) {
  return state.lobby;
}

export default withRouter(connect(mapStateToProp)(LobbyContainer));
