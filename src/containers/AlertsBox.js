import React, { Component } from "react";
import { connect } from "react-redux";

class AlertsBox extends Component {
  render() {
    const { alert } = this.props;
    if (!alert) return <div />;
    return (
      <div className="alertsBox">
        <div className="alert">{this.props.alert}</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    alert: state.game.alert,
  };
}

export default connect(mapStateToProps)(AlertsBox);
