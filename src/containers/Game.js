import React, { Component } from "react";

import ScoreDisplaying from "./ScoreDisplaying";
import Grid from "./Grid";

class GameContainer extends Component {
  state = {};

  // - Rendering

  render() {
    return (
      <div className="gameContainer">
        <ScoreDisplaying />
        <Grid />
      </div>
    );
  }
}

export default GameContainer;
