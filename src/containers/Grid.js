import React, { Component } from "react";
import Cell from "../components/Cell";
import Chat from "../components/Chat";
import AlertsBox from "./AlertsBox";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Sound from "react-sound";

// import NotificationSound from "../assets/notification.mp3";

import {
  selectPiece,
  updateGame,
  createGrid,
  regainEnergy,
  displayAlert,
  restartGame,
  startCountdown,
  endCountdown,
} from "../actions/game";
import { socket } from "../containers/Main";
import { withRouter } from "react-router-dom";
import ReadyModal from "../components/ReadyModal";
import EndGameModal from "../components/EndGameModal";
import ReconnectionModal from "../components/ReconnectionModal";

import { retrieveSession } from "../services/SessionService";
import CountdownModal from "../components/CountdownModal";

import { Popconfirm } from "antd";

class Grid extends Component {
  constructor(props) {
    super(props);

    this.state = {
      reconnectionModal: false,
      pseudoReconnection: "",
      soundStatus: Sound.status.STOPPED,
    };
  }

  unsubscribe() {
    socket.removeAllListeners("reconnect");
    socket.removeAllListeners("game_created");
    socket.removeAllListeners("game_paused");
    socket.removeAllListeners("game_unpaused");
    socket.removeAllListeners("update");
    socket.removeAllListeners("cancelMove");
    socket.removeAllListeners("updateEnergy");
    socket.removeAllListeners("ready");
    socket.removeAllListeners("disconnect");
    socket.removeAllListeners("endCountdown");
  }
  componentDidMount() {
    if (socket === undefined) {
      return this.props.history.push("/game/lobby");
    }

    this.unsubscribe();

    socket.on("game_created", (data) => {
      if (data.game.players[0].id === socket.id) {
        this.setState({ soundStatus: Sound.status.PLAYING });
        document.title = "Adversaire trouvé !";
      }
      this.props.createGrid(data);
    });

    socket.on("game_paused", (data) => {
      this.setState({
        reconnectionModal: true,
        pseudoReconnection: data.pseudo,
      });
    });

    socket.on("endCountdown", () => {
      this.props.endCountdown();
    });

    socket.on("game_unpaused", (data) => {
      this.setState({ reconnectionModal: false });
      if (!data.end && !this.props.countdown.onGoing) {
        this.props.startCountdown();
      }
    });

    const session = retrieveSession();
    const { username } = session.user;
    socket.on("disconnect", () => {
      this.setState({ reconnectionModal: true, pseudoReconnection: username });
    });

    socket.on("reconnect", () => {
      this.setState({ reconnectionModal: false });
      if (this.props.game) {
        socket.emit("joinRoom", {
          id: this.props.game.id,
          pseudo: username,
        });
      } else {
        const { pathname } = this.props.history.location;
        if (pathname === "/game/game") {
          alert("Déconnecté. Relance la recherche frérot");
        }
        this.unsubscribe();

        this.props.history.push("/game/lobby");
      }
    });

    socket.on("update", (data) => {
      document.title = "Dupz";
      this.props.updateGame(data);
    });

    socket.on("cancelMove", () => {
      this.props.displayAlert("Ton coup a été annulé !");
    });

    socket.on("updateEnergy", (data) => {
      this.props.regainEnergy(data.id);
    });
  }

  renderGrid() {
    const { grid, selectPiece, updateGame, jumpPiece, countdown } = this.props;
    return grid.map((cell, i = 0) => {
      return (
        <Cell
          countdownOnGoing={countdown.onGoing || this.state.reconnectionModal}
          key={i++}
          cell={cell}
          selectPiece={selectPiece}
          updateGame={updateGame}
          jumpPiece={jumpPiece}
          endDetection={this.props.endDetection}
          regainEnergy={this.props.regainEnergy}
          displayAlert={this.props.displayAlert}
        />
      );
    });
  }

  handleSongFinishedPlaying = () => {
    this.setState({ soundStatus: Sound.status.STOPPED });
  };
  confirm = () => {
    const { game, history } = this.props;

    this.unsubscribe();
    socket.emit("leaveRoom", { gameId: game.id });
    history.push(`/game/lobby`);
  };
  render() {
    const { game, grid, countdown, history } = this.props;

    let nbColumns = 0;
    if (!game)
      return (
        <div>
          <button
            className="btn btn-primary"
            onClick={() => {
              this.unsubscribe();
              socket.emit("leaveRoom");
              history.push(`/game/lobby`);
            }}
          >
            Annuler
          </button>
          En attente d'adversaire...
        </div>
      );

    nbColumns = grid[grid.length - 1].coords.j + 1;

    const isMobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(navigator.userAgent||navigator.vendor||window.opera)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw(n|u)|c55\/|capi|ccwa|cdm|cell|chtm|cldc|cmd|co(mp|nd)|craw|da(it|ll|ng)|dbte|dcs|devi|dica|dmob|do(c|p)o|ds(12|d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(|_)|g1 u|g560|gene|gf5|gmo|go(\.w|od)|gr(ad|un)|haie|hcit|hd(m|p|t)|hei|hi(pt|ta)|hp( i|ip)|hsc|ht(c(| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i(20|go|ma)|i230|iac( ||\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|[a-w])|libw|lynx|m1w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|mcr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|([1-8]|c))|phil|pire|pl(ay|uc)|pn2|po(ck|rt|se)|prox|psio|ptg|qaa|qc(07|12|21|32|60|[2-7]|i)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h|oo|p)|sdk\/|se(c(|0|1)|47|mc|nd|ri)|sgh|shar|sie(|m)|sk0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h|v|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl|tdg|tel(i|m)|tim|tmo|to(pl|sh)|ts(70|m|m3|m5)|tx9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas|your|zeto|zte/i.test((navigator.userAgent||navigator.vendor||window.opera).substr(0,4))
    const styleColumns = isMobile ? "10.88vw " : "60px ";
    return (
      <div className="game">
        <div className="lobbyButton">
          {game.endGame ? (
            <button
              className="btn btn-primary"
              onClick={this.confirm}
            >
              Retourner au lobby
            </button>
          ) : (
            <Popconfirm
              title="Abandonner la partie?"
              onConfirm={this.confirm}
              okText="Confirmer"
              cancelText="Annuler"
            >
              <button className="btn btn-primary">
                Retourner au lobby
              </button>
            </Popconfirm>
          )}
        </div>
        {/* <Sound
          url={NotificationSound}
          playStatus={this.state.soundStatus}
          playFromPosition={0}
          onFinishedPlaying={this.handleSongFinishedPlaying}
        />
        */}

        <ReadyModal gameId={game.id} visible={!game.started} />

        {countdown.onGoing ? (
          <CountdownModal title={countdown.title} />
        ) : (
          <div />
        )}

        <ReconnectionModal
          visible={this.state.reconnectionModal && !game.endGame}
          pseudo={this.state.pseudoReconnection}
        />

        {game.endGame ? (
          <EndGameModal
            gameId={game.id}
            type={game.type}
            restartGame={this.props.restartGame}
            history={history}
            winner={game.winner}
          />
        ) : (
          ""
        )}
        <div className="gridAndChat">
          <div
            className="grid"
            style={{
              gridTemplateColumns: styleColumns.repeat(nbColumns),
            }}
          >
            {this.renderGrid()}
          </div>
          <Chat />
        </div>
        <AlertsBox />
      </div>
    );
  }
}
function mapStateToProp(state) {
  let grid;
  if (state.game.game) {
    grid = state.game.game.grid.grid;
  }
  return {
    grid: grid,
    game: state.game.game,
    countdown: state.game.countdown,
    gameHasStarted: state.game.gameHasStarted,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectPiece,
      updateGame,
      createGrid,
      regainEnergy,
      displayAlert,
      restartGame,
      startCountdown,
      endCountdown,
    },
    dispatch
  );
}

export default withRouter(
  connect(
    mapStateToProp,
    mapDispatchToProps
  )(Grid)
);
