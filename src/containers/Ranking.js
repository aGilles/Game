import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Switch, Route, withRouter } from "react-router-dom";

import { getDupzRanking, getFunRanking } from "../actions/ranking";

import Ranking from "../components/Ranking";

const RankingContainer = (props) => {
  const { dataDupz, dataFun, getDupzRanking, getFunRanking, history } = props;

  useEffect(() => {
    getDupzRanking();
    getFunRanking();
  }, []);

  return (
    <Switch>
      <Route
        path="/ranking/dupz"
        component={() => (
          <Ranking data={dataDupz} history={history} type="Dupz" />
        )}
      />
      <Route
        path="/ranking/fun"
        component={() => (
          <Ranking data={dataFun} history={history} type="Fun" />
        )}
      />
    </Switch>
  );
};

function mapStateToProp(state) {
  return state.ranking;
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getDupzRanking,
      getFunRanking,
    },
    dispatch
  );
}

export default withRouter(
  connect(
    mapStateToProp,
    mapDispatchToProps
  )(RankingContainer)
);
