import React from "react";
import "./header.css";

const Header = () => {
  return (
    <div className="header">
      <div className="left">
        <div className="discord">
          <img
            src={require("../../assets/images/discord.png")}
            width={40}
            height={40}
            alt={"discord"}
          />
          <div className="textIcon">
            <a
              href="https://discord.gg/rQV4Q6S"
              target="_blank"
              rel="noopener noreferrer"
            >
              Rejoins la communauté sur le serveur discord Dupz !
            </a>
          </div>
        </div>

        <div className="youtube">
          <img
            src={require("../../assets/images/youtube.png")}
            width={40}
            height={40}
            alt={"youtube"}
          />
          <div className="textIcon">
            <a target="_blank">Comment jouer ?</a>
          </div>
        </div>
      </div>

      <div className="dupz">Dupz</div>

      <div className="right" />
    </div>
  );
};

export default Header;
