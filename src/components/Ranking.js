import React from "react";
import { Table } from "antd";

const Ranking = (props) => {
  const { data, history, type } = props;

  const otherType = type === "Dupz" ? "Fun" : "Dupz";

  const columns = [
    {
      title: "Rank",
      dataIndex: "rank",
      key: "rank",
    },
    {
      title: "Pseudo",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "ELO",
      dataIndex: "rating",
      key: "rating",
    },
  ];

  const renderHeader = () => {
    return (
      <div>
        <div style={{ display: "flex" }}>
          <button
            className="btn btn-primary"
            onClick={() => {
              history.push(`/game/lobby`);
            }}
          >
            Lobby
          </button>
          <div style={{ width: "3vw" }} />
          <button
            className="btn btn-primary"
            onClick={() => {
              history.push(`/ranking/${otherType}`);
            }}
          >
            Classement {otherType}
          </button>
        </div>
        <div
          style={{
            display: "flex",
            fontWeight: "bold",
            fontSize: "3rem",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <div>Classement {type}</div>
        </div>
      </div>
    );
  };
  return (
    <div>
      <Table
        bordered
        size="middle"
        title={renderHeader}
        columns={columns}
        dataSource={data.data}
      />
    </div>
  );
};

export default Ranking;
