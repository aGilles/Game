import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { socket } from "../containers/Main";

import userService from "../services/UserService";
import { retrieveSession } from "../services/SessionService";

import "../assets/css/Home.css";
import CardMode from "./CardMode/CardMode";
import Header from "./Header/header";

class Lobby extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      username: "",
    };
  }

  componentDidMount() {
    const session = retrieveSession();

    this.setState({ isAuthenticated: true, username: session.user.username });

    socket.on("reconnect_attempt", () => {
      socket.io.opts.transports = ["polling", "websocket"];
    });
    userService.setUser(session.user);

    //socket.on("404", () => console.log("not an user!"));
  }

  startGame(value, history) {
    const session = retrieveSession();
    const { username } = session.user;
    socket.emit("create_game", {
      pseudo: username,
      type: value === "classique" ? "bacteria" : value,
    });
    history.push("/game/game");
  }

  // disconnect() {
  //   destroySession();
  //   this.setState({ isAuthenticated: false });

  //   const { history } = this.props;
  //   history.push("/login");
  //   this.unsubscribe();
  //   socket.disconnect();
  // }

  unsubscribe() {
    socket.removeAllListeners("onGoing");
    socket.removeAllListeners("crushingConnection");
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
  };

  render() {
    const {
      numberUsers,
      // numberGames,
      numberGamesWaitingDupz,
      numberGamesOnGoingDupz,
      numberGamesWaitingFun,
      numberGamesOnGoingFun,
      numberGamesWaitingBacteria,
      numberGamesOnGoingBacteria,
      // usernames,
      dupzGames,
      funGames,
      bacteriaGames,
      history,
    } = this.props;

    if (!this.state.isAuthenticated) {
      return <div />;
    }
    return (
      <div className="lobby">
        <Header />
        <div className="cardsContainer">
          <CardMode
            type="dupz"
            history={history}
            unsubscribe={this.unsubscribe}
            gamesWaiting={numberGamesWaitingDupz}
            gamesOnGoing={numberGamesOnGoingDupz}
            startGame={this.startGame}
            players={dupzGames}
          />

          <CardMode
            type="fun"
            history={history}
            unsubscribe={this.unsubscribe}
            gamesWaiting={numberGamesWaitingFun}
            gamesOnGoing={numberGamesOnGoingFun}
            startGame={this.startGame}
            players={funGames}
          />
          <CardMode
            type="classique"
            history={history}
            unsubscribe={this.unsubscribe}
            gamesWaiting={numberGamesWaitingBacteria}
            gamesOnGoing={numberGamesOnGoingBacteria}
            startGame={this.startGame}
            players={bacteriaGames}
          />
        </div>
        <div
          className="nbPlayers"
        >
          {numberUsers}{" "}
          {numberUsers > 1 ? "joueurs connectés" : "joueur connecté"}
          {/* <button
            className="btn btn-primary"
            onClick={this.disconnect.bind(this)}
          >
            Se déconnecter
          </button> */}
        </div>
      </div>
    );
  }
}

export default withRouter(Lobby);
