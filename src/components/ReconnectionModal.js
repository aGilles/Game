import React from "react";
import { Modal, Statistic } from "antd";

const ReconnectionModal = (props) => {
  const { visible, pseudo } = props;
  const renderContentModal = () => {
    const { Countdown } = Statistic;
    return (
      <div className="ReconnectionModal">
        En attente de {pseudo} ...
        <Countdown value={Date.now() + 30500} format="s" />
      </div>
    );
  };

  return (
    <div>
      <Modal
        visible={visible}
        title="Reconnection"
        cancelButtonProps={{ style: { display: "none" } }}
        okButtonProps={{ style: { display: "none" } }}
      >
        {renderContentModal()}
      </Modal>
    </div>
  );
};

export default ReconnectionModal;
