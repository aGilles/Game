import React, { useEffect } from "react";
import { Statistic } from "antd";
import { message } from "antd";

message.config({
  top: 30,
  maxCount: 1,
});
const CountdownModal = (props) => {
  const { title } = props;

  useEffect(() => {
    message.info({
      content: renderContentModal(),
      duration: 5,
    });
  });

  const renderContentModal = () => {
    const { Countdown } = Statistic;

    return (
      <div>
        {title} de la partie dans :
        <Countdown value={Date.now() + 4500} format="s" />
      </div>
    );
  };

  return <div />;
};

export default CountdownModal;
