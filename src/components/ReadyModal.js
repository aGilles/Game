import React, { useState, useEffect } from "react";
import { Modal, Button } from "antd";
import PlayerReady from "./PlayerReady";
import { socket } from "../containers/Main";

const ReadyModal = (props) => {
  const [player1Ready, setPlayer1Ready] = useState(false);
  const [player2Ready, setPlayer2Ready] = useState(false);

  useEffect(() => {
    socket.on("ready", (data) => {
      if (data.player1) {
        setPlayer1Ready(true);
      } else {
        setPlayer2Ready(true);
      }
      socket.removeAllListeners("ready");
    });
  });

  const handleReadyClick = () => {
    socket.emit("ready", { gameId: props.gameId });
  };

  //   const { confirm } = Modal;

  const renderContentModal = () => {
    return (
      <div className="readyModal">
        <PlayerReady username="Joueur 1" ready={player1Ready} />
        <div style={{ width: "5vw" }} />
        <PlayerReady username="Joueur 2" ready={player2Ready} />
      </div>
    );
  };

  //   function showConfirm() {
  //     confirm({
  //       cancelButtonProps: { style: { display: "none" } },
  //       title: "La partie va commencer",
  //       content: renderContentModal(),
  //       okText: "Je suis prêt",
  //       onOk() {
  //         handleReadyClick();
  //       },
  //     });
  //   }

  return (
    <div>
      <Modal
        visible={props.visible}
        title="La partie va commencer"
        onOk={handleReadyClick}
        okText="Je suis prêt"
        cancelButtonProps={{ style: { display: "none" } }}
        footer={[
          <Button key="ready" onClick={handleReadyClick}>
            Je suis prêt
          </Button>,
        ]}
      >
        {renderContentModal()}
      </Modal>
    </div>
  );
};

export default ReadyModal;
