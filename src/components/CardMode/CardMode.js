import React, { useState } from "react";
import { Card, Table } from "antd";
import { Tooltip } from "antd";
import "antd/dist/antd.css";
import "./cardMode.css";
import { FaArrowAltCircleRight, FaArrowAltCircleLeft } from "react-icons/fa";

const CardMode = (props) => {
  const {
    type,
    history,
    unsubscribe,
    gamesWaiting,
    gamesOnGoing,
    startGame,
    players,
  } = props;
  const [tab, setTab] = useState("games");

  const renderGames = () => {
    return (
      <div className="cardContent">
        <div className="alignLeftClass">
          <div>{gamesWaiting} partie en attente</div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
            }}
          >
            <div>{gamesOnGoing} parties en cours</div>
            <div style={{ paddingLeft: "2vw", paddingBottom: "0" }}>
              <Tooltip title="Voir les parties en cours">
                <button
                  className="seeGames"
                  onClick={() => setTab("gamesOnGoing")}
                  style={{ borderWidth: 0 }}
                >
                  <FaArrowAltCircleRight color={"blue"} size={"4vh"} />
                </button>
              </Tooltip>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const renderGamesOnGoing = () => {
    const columns = [
      {
        title: "Joueur 1",
        dataIndex: "player1",
        key: "player1" + type,
      },
      {
        title: "Joueur 2",
        dataIndex: "player2",
        key: "player2" + type,
      },
    ];
    return (
      <div>
        <button onClick={() => setTab("games")} style={{ borderWidth: 0 }}>
          <FaArrowAltCircleLeft color={"blue"} size={"4vh"} />
        </button>
        <Table bordered size="middle" columns={columns} dataSource={players} />
      </div>
    );
  };

  const renderLeaderboard = () => {};

  const renderCardContent = () => {
    switch (tab) {
      case "games":
        return renderGames();
      case "gamesOnGoing":
        return renderGamesOnGoing();
      case "leaderboard":
        return renderLeaderboard();
      default:
        return;
    }
  };

  const handleOnClickLeaderboard = () => {
    unsubscribe();
    history.push("/ranking/" + type);
  };
  return (
    <div
      className="cardContainer"
    >
      <Card
        className="myCard"
        title={
          <div className="titleCard">
            {type.charAt(0).toUpperCase() + type.slice(1)}
          </div>
        }
        extra={
          type !== "classique" ? (
            <Tooltip title="Voir le classement">
              <button
                onClick={handleOnClickLeaderboard}
                style={{ borderWidth: 0 }}
              >
                <img
                  src={require("../../assets/images/ranking.png")}
                  width={40}
                  height={40}
                  alt={"ranking"}
                />
              </button>
            </Tooltip>
          ) : (
            ""
          )
        }
      >
        <div className="contentClass">{renderCardContent()}</div>
        <div className="footer">
          <button
            className="btn btn-primary startButton"
            onClick={() => {
              startGame(type, history);
            }}
          >
            Lancer une partie
          </button>
        </div>
      </Card>
    </div>
  );
};

export default CardMode;
