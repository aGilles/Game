import React, { Component } from "react";
import { Type } from "../models/Grid";

// CE CODE EST HONTEUX
class Piece extends Component {
  render() {
    const { type, shield, isSelected } = this.props;
    let className = "";
    if (type === Type.PLAYER1) {
      if (shield === 0) {
        if (isSelected) {
          className = "selectedPiecePlayer1";
        } else {
          className = "piecePlayer1";
        }
      } else {
        if (isSelected) {
          className = "selectedShieldPlayer1";
        } else {
          className = "shieldPlayer1";
        }
      }
    } else {
      if (shield === 0) {
        if (isSelected) {
          className = "selectedPiecePlayer2";
        } else {
          className = "piecePlayer2";
        }
      } else {
        if (isSelected) {
          className = "selectedShieldPlayer2";
        } else {
          className = "shieldPlayer2";
        }
      }
    }

    return (
      <div className={className}>
        <div className="piecetest" />
      </div>
    );
  }
}

export default Piece;
