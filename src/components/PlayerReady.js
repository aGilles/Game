import React from "react";
import { FaSpinner, FaCheck } from "react-icons/fa";

const PlayerReady = (props) => {
  const { username, ready } = props;

  return (
    <div>
      <div>{username}</div>
      <div>{ready ? <FaCheck /> : <FaSpinner />}</div>
    </div>
  );
};

export default PlayerReady;
