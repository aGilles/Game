import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { newMessage } from "../actions/game";
import { bindActionCreators } from "redux";
import { socket } from "../containers/Main";
import { Input } from "antd";

const { TextArea } = Input;
const ChatComponent = (props) => {
  const [messages, setMessages] = useState([]);
  const [otherLeft, setOtherLeft] = useState(false);
  useEffect(() => {
    socket.on("messageGame", (data) => {
      let className;
      if (data.id === socket.id) {
        className = "expeditor";
      } else {
        className = "destinator";
      }
      const newMessages = [...messages, { className, content: data.content }];
      setMessages(newMessages);
      socket.removeAllListeners("messageGames");
    });

    socket.on("otherLeftRoom", () => {
      setOtherLeft(true);
      socket.removeAllListeners("otherLeftRoom");
    });
  });

  const [inputValue, setInputValue] = useState("");

  const renderMessages = () => {
    return messages.map((message) => (
      <div
        className="message"
        style={{
          height: Math.ceil(message.content.length / 20) * 3 + 3 + "vh",
        }}
        key={message.content}
      >
        <div className={message.className}>{message.content}</div>
      </div>
    ));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    socket.emit("messageGame", { gameId: props.game.id, content: inputValue });
    setInputValue("");
  };

  return (
    <div className="chatContainer">
      <div className="chat">{renderMessages()}</div>
      <div className="bottomChat">
        {otherLeft ? (
          <div style={{ color: "red", fontWeight: "bold", textAlign: "right" }}>
            L'autre joueur a quitté le salon.
          </div>
        ) : (
          <div />
        )}
        <form className="formChat" onSubmit={handleSubmit}>
          <TextArea
            onPressEnter={handleSubmit}
            rows={3}
            className="inputChat"
            type="text"
            value={inputValue}
            onChange={(event) => setInputValue(event.target.value)}
            placeholder={"Envoyer un message..."}
          />
          <input
            className="submitChat"
            type="submit"
            disabled={otherLeft}
            value="Envoyer"
          />
        </form>
      </div>
    </div>
  );
};

const mapStateToProp = (state) => {
  return {
    game: state.game.game,
    messages: state.game.messages,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ newMessage }, dispatch);
};

export default connect(
  mapStateToProp,
  mapDispatchToProps
)(ChatComponent);
