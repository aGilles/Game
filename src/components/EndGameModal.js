import React, { useState } from "react";
import { Modal, Button } from "antd";
import { socket } from "../containers/Main";
import { retrieveSession } from "../services/SessionService";

const EndGameModal = (props) => {
  const [visible, setVisible] = useState(true);

  //   const { confirm } = Modal;

  const { restartGame, gameId, type, history, winner } = props;
  const renderContentModal = () => {
    return (
      <div className="endGameModal">
        {winner ? `Victoire de ${winner.name}` : "Match nul."}
      </div>
    );
  };

  const handleGoBackToLobby = () => {
    setVisible(false);
    socket.emit("leaveRoom", { gameId });
    socket.removeAllListeners("reconnect");
    socket.removeAllListeners("game_created");
    socket.removeAllListeners("game_paused");
    socket.removeAllListeners("game_unpaused");
    socket.removeAllListeners("update");
    socket.removeAllListeners("cancelMove");
    socket.removeAllListeners("updateEnergy");
    socket.removeAllListeners("ready");
    socket.removeAllListeners("disconnect");
    socket.removeAllListeners("endCountdown");

    restartGame();
    history.push("/game/lobby");
  };

  const handleRestart = () => {
    setVisible(false);

    const session = retrieveSession();

    const pseudo = session.user.username;

    socket.emit("leaveRoom", { gameId });
    socket.emit("create_game", { pseudo, type });
    restartGame();
  };

  //   function showConfirm() {
  //     confirm({
  //       cancelButtonProps: { style: { display: "none" } },
  //       title: "La partie va commencer",
  //       content: renderContentModal(),
  //       okText: "Je suis prêt",
  //       onOk() {
  //         handleReadyClick();
  //       },
  //     });
  //   }

  return (
    <div>
      <Modal
        visible={visible}
        title="Fin de la partie"
        onCancel={() => {
          setVisible(false);
        }}
        footer={[
          <Button key="lobby" onClick={handleGoBackToLobby}>
            Retourner au lobby
          </Button>,
          <Button key="restart" onClick={handleRestart}>
            Restart
          </Button>,
        ]}
      >
        {renderContentModal()}
      </Modal>
    </div>
  );
};

export default EndGameModal;
