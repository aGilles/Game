import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { Button } from 'antd';

import '../assets/css/Authform.css'

class AuthForm extends Component {
  // - User actions

  onConnection () {
    const {startLogin} = this.props
    startLogin()
  }

  onRegisteration () {
    const {startRegistration} = this.props
    startRegistration()
  }

  // - Rendering

  renderField (field) {
    const {
      type,
      meta: {touched, error},
    } = field //equals const touched = field.meta.touched and same for error
    const className = `form-group ${touched && error ? 'has-danger' : ''}`
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className="form-control"
          type={type}
          {...field.input} // equals all the onX like onChange={field.input.onChange}
        />
        <div className="text-help">{touched ? error : ''}</div>
      </div>
    )
  }

  _renderRegistrationForm () {
    const {handleSubmit, onSubmit} = this.props
    return (
      <div className="auth">
        <form onSubmit={handleSubmit(onSubmit)} className="authForm">
          <Field label="Pseudo" name="username" component={this.renderField}/>
          <Field
            label="Mot de passe"
            type="password"
            name="password"
            component={this.renderField}
          />
          <Field
            label="Confirmer mot de passe"
            type="password"
            name="passwordConfirmation"
            component={this.renderField}
          />
          <div className="bntClass">
            <button type="submit" className="btn btn-primary">
              S'inscrire
            </button>
          </div>
        </form>
        <Button type="link" onClick={this.onConnection.bind(this)} block className="insBnt">
          Déjà inscrit ? Se connecter
        </Button>
      </div>
    )
  }

  _renderLoginForm () {
    const {handleSubmit, onSubmit} = this.props
    return (
      <div className="auth">
        <form onSubmit={handleSubmit(onSubmit)} className="authForm">
          <Field className="fieldInput" label="Pseudo" name="username" component={this.renderField}/>
          <Field
            className="fieldInput"
            label="Mot de passe"
            name="password"
            type="password"
            component={this.renderField}
          />
          <div className="bntClass">
            <button type="submit" className="btn btn-primary">
              Se connecter
            </button>
          </div>
        </form>
        <Button type="link"  onClick={this.onRegisteration.bind(this)} block className="insBnt">
          Pas inscrit ? S'inscrire
        </Button>
      </div>
    )
  }

  render () {
    const {isRegistering} = this.props
    if (!isRegistering) {
      return this._renderLoginForm()
    } else {
      return this._renderRegistrationForm()
    }
  }
}

function validate (values) {
  const errors = {}

  //validate the inputs from 'values'
  if (!values.username) {
    errors.username = 'Entre un pseudo wesh'
  } else if (values.username.length > 11) {
    errors.username = '10 caractères max'
  }

  if (!values.password) {
    errors.password = 'Entre un mot de passe wesh'
  }

  if (values.passwordConfirmation !== values.password) {
    errors.passwordConfirmation = 'Erreur confirmation mot de passe '
  }
  //if errors is empty, the form is fine to submit
  //if errors has any property, redux form assumes form is invalid
  return errors
}

export default reduxForm({
  form: 'PostsNewForm',
  validate,
})(AuthForm)
