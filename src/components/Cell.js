import React, { Component } from "react";
import Piece from "./Piece";
import { connect } from "react-redux";
import Grid, { Type } from "../models/Grid";
import Hole from "./Hole";
import { socket } from "../containers/Main";
import ClickNHold from "react-click-n-hold";

class Cell extends Component {
  renderCell() {
    const {
      cell,
      selectedCell,
      players,
      game,
      possibleDups,
      possibleJumps,
    } = this.props;
    const { type, shield } = this.props.cell;

    if (type === Type.PLAYER1 || type === Type.PLAYER2) {
      return (
        <Piece
          type={type}
          shield={shield}
          isSelected={selectedCell && selectedCell.id === cell.id}
        />
      );
    }
    if (!selectedCell) return <div className="emptyCell" />;

    const idPlayerPlaying = selectedCell.type;
    const energyPlayer = players[idPlayerPlaying].energy;
    const canDup = possibleDups.find(
      (cell2) =>
        cell.coords.i === cell2.coords.i && cell.coords.j === cell2.coords.j
    );
    if (canDup) {
      if (energyPlayer >= 1 || game.type !== "dupz") {
        return <div className={`canDupPlayer${idPlayerPlaying + 1}`} />;
      } else return <div className={`cantDupPlayer${idPlayerPlaying + 1}`} />;
    }

    const canJump = possibleJumps.find(
      (cell2) =>
        cell.coords.i === cell2.coords.i && cell.coords.j === cell2.coords.j
    );
    if (canJump) {
      if (energyPlayer >= 1 || game.type !== "dupz") {
        return <div className={`canJumpPlayer${idPlayerPlaying + 1}`} />;
      } else return <div className={`cantJumpPlayer${idPlayerPlaying + 1}`} />;
    }

    return <div className="emptyCell" />;
  }

  onCellClicked() {
    const {
      cell,
      selectedCell,
      players,
      endGame,
      started,
      game,
      countdownOnGoing,
    } = this.props;
    if (cell.type === Type.HOLE || endGame || !started || countdownOnGoing)
      return;

    if (game.lastPlayerPlayed === socket.id && game.type === "bacteria") {
      return;
    }

    if (
      game.lastPlayerPlayed === undefined &&
      game.firstPlayer === socket.id &&
      game.type === "bacteria"
    ) {
      return;
    }

    // If the cell contains a piece and it belongs to the player
    if (cell.idPlayer === socket.id) {
      return this.props.selectPiece(cell);
    }

    // If there is not a cell selected (and the cell doesn't contains a piece)
    // then returns
    if (!selectedCell || cell.type !== Type.EMPTY) {
      return;
    }

    //Try to duplicate a piece
    const isCellNotTheSelectedOne = selectedCell !== cell;
    const isCellAdjacentToSelectedOne = Grid.adjacentTest(selectedCell, cell);
    const idPlayerPlaying = selectedCell.type;
    const energyPlayer = players[idPlayerPlaying].energy;

    if (energyPlayer < 1 && game.type === "dupz") {
      return this.props.displayAlert("Pas assez d'énergie !!");
    }
    if (isCellNotTheSelectedOne && isCellAdjacentToSelectedOne) {
      return socket.emit("duplication", {
        cellFrom: selectedCell,
        cellTo: cell,
        gameId: this.props.game.id,
      });
    }

    //Try to jump the piece
    const isCellAtRangeOfJump = Grid.jumpTest(selectedCell, cell);
    if (isCellNotTheSelectedOne && isCellAtRangeOfJump) {
      return socket.emit("jump", {
        cellFrom: selectedCell,
        cellTo: cell,
        gameId: this.props.game.id,
      });
    }
  }

  onCellRightClicked(event) {
    event.preventDefault();

    this.shield();
  }

  shield() {
    const { cell, endGame, started, players, game } = this.props;
    if (game.type === "bacteria") {
      return;
    }
    const player = players.find((player) => player.id === socket.id);
    if (player.energy < 1 && game.type === "dupz") {
      return this.props.displayAlert("Pas assez d'énergie !!");
    }
    if (
      !started ||
      cell.shield === 1 ||
      cell.type === Type.HOLE ||
      endGame ||
      cell.type === Type.EMPTY ||
      cell.idPlayer !== socket.id
    ) {
      return;
    }
    socket.emit("shield", { cell, gameId: this.props.game.id });
  }

  onEndClickHold = () => {
    this.shield();
  };

  render() {
    if (this.props.cell.type === Type.HOLE) {
      return (
        <div className="cell">
          <Hole />
        </div>
      );
    }
    // if (_.isEqual(this.props.selectedCell, this.props.cell)) {
    // }

    return (
      <ClickNHold
        time={0.5} // Time to keep pressing.
        onClickNHold={this.onEndClickHold} //Timeout callback
      >
        <div
          onClick={this.onCellClicked.bind(this)}
          onContextMenu={(event) => {
            this.onCellRightClicked(event);
          }}
          className="cell"
        >
          {this.renderCell()}
        </div>
      </ClickNHold>
    );
  }
}

function mapStateToProp(state) {
  return {
    selectedCell: state.game.selectedCell,
    players: state.game.game.players,
    endGame: state.game.game.endGame,
    started: state.game.game.started,
    game: state.game.game,
    possibleDups: state.game.possibleDups,
    possibleJumps: state.game.possibleJumps,
  };
}

export default connect(mapStateToProp)(Cell);
