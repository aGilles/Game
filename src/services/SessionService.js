import { Session } from "../models/Session";
import User from "../models/User";

const USER_ID_KEY = "userId";
const TOKEN_KEY = "token";
const USERNAME_KEY = "username";

export function createSession(user, token) {
  const session = new Session(user, token);
  window.localStorage.setItem(USER_ID_KEY, session.user.userId);
  window.localStorage.setItem(TOKEN_KEY, session.token);
  window.localStorage.setItem(USERNAME_KEY, session.user.username);
  return session;
}

export function retrieveSession() {
  const userId = window.localStorage.getItem(USER_ID_KEY);
  const token = window.localStorage.getItem(TOKEN_KEY);
  const username = window.localStorage.getItem(USERNAME_KEY);

  const user = new User(username, userId);

  const session = new Session(user, token);
  return session;
}

export function destroySession() {
  window.localStorage.setItem(USER_ID_KEY, null);
  window.localStorage.setItem(TOKEN_KEY, null);
  window.localStorage.setItem(USERNAME_KEY, null);
}
