import * as actions from "../actions/auth";

const defaultState = {
  processing: false,
  error: null,
  session: null
};

export default function(state = defaultState, { type, payload }) {
  switch (type) {
    case actions.AUTHENTICATE_STARTED:
      return { ...state, processing: true, error: null, session: null };
    case actions.AUTHENTICATE_SUCCEEDED:
      return {
        ...state,
        processing: false,
        error: null,
        session: payload.session
      };
    case actions.AUTHENTICATE_FAILED:
      return {
        ...state,
        processing: false,
        error: payload.error,
        session: null
      };
    default:
      return state;
  }
}
