import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import authenticationReducer from "./authentication";
import lobbyReducer from "./lobby";
import rankingReducer from "./ranking";

import gameReducer from "./game";

const rootReducer = combineReducers({
  authentication: authenticationReducer,
  lobby: lobbyReducer,
  game: gameReducer,
  form: formReducer,
  ranking: rankingReducer,
});

export default rootReducer;
