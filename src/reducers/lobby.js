import * as actions from "../actions/lobby";

const defaultState = {
  numberUsers: 0,
  numberGames: 0,
  numberGamesWaitingDupz: 0,
  numberGamesWaitingBacteria: 0,
  numberGamesWaitingFun: 0,
  numberGamesOnGoingFun: 0,
  numberGamesOnGoingDupz: 0,
  numberGamesOnGoingBacteria: 0,
  usernames: [],
  dupzGames: [],
  funGames: [],
  bacteriaGames: [],
};

export default function(state = defaultState, { type, payload }) {
  switch (type) {
    case actions.UPDATE_NUMBERS:
      return payload;
    default:
      return state;
  }
}
