import {
  UPDATE_GAME,
  SELECT_CELL,
  CREATE_GRID,
  REGAIN_ENERGY,
  DISPLAY_ALERT,
  NEW_MESSAGE_CHAT,
  RESTART_GAME,
  START_COUNTDOWN,
  END_COUNTDOWN,
} from "../actions/game";
import Game from "../models/Game";
import Player from "../models/Player";
import Grid, { Type } from "../models/Grid";
import { socket } from "../containers/Main";

export default function(
  state = {
    messages: [],
    gameHasStarted: false,
    countdown: { onGoing: false, title: "" },
  },
  action
) {
  switch (action.type) {
    case CREATE_GRID:
      let game = modeliseGame(action.payload.game);
      return { ...state, game, selectedCell: null };
    case SELECT_CELL:
      const selectedCell = action.payload;
      const possibleDups = getPossibleDups(
        action.payload,
        state.game.grid.grid
      );
      const possibleJumps = getPossibleJumps(
        action.payload,
        state.game.grid.grid
      );
      return { ...state, selectedCell, possibleDups, possibleJumps };
    case UPDATE_GAME:
      let newGame = modeliseGame(action.payload.game, state);

      if (
        !state.gameHasStarted &&
        newGame.started &&
        newGame.type !== "bacteria"
      ) {
        state = {
          ...state,
          countdown: {
            title: "Début",
            onGoing: true,
          },
          gameHasStarted: true,
        };
      }
      let possibleDupsUpdate = [];
      let possibleJumpsUpdate = [];

      if (socket.id === newGame.lastPlayerPlayed) {
        state = {
          ...state,
          selectedCell: null,
          alert: null,
        };
      } else if (
        state.selectedCell &&
        state.selectedCell.idPlayer ===
          newGame.grid.grid.find((cell) => cell.id === state.selectedCell.id)
            .idPlayer
      ) {
        possibleDupsUpdate = getPossibleDups(
          state.selectedCell,
          newGame.grid.grid
        );
        possibleJumpsUpdate = getPossibleJumps(
          state.selectedCell,
          newGame.grid.grid
        );
      }

      return {
        ...state,
        game: newGame,
        possibleDups: possibleDupsUpdate,
        possibleJumps: possibleJumpsUpdate,
      };

    case REGAIN_ENERGY:
      let idPlayer2 = action.payload;
      let newGame2 = Object.assign(new Game(), state.game);
      let player = newGame2.players[idPlayer2];
      player.incrementEnergy();
      return { ...state, game: newGame2 };

    case DISPLAY_ALERT:
      return { ...state, alert: action.payload };

    case NEW_MESSAGE_CHAT:
      const newMessages = [...state.messages];

      newMessages.push(action.payload);
      return { ...state, messages: newMessages };

    case RESTART_GAME:
      return { ...state, game: undefined, gameHasStarted: false };
    case START_COUNTDOWN:
      return { ...state, countdown: { title: "Reprise", onGoing: true } };

    case END_COUNTDOWN:
      return { ...state, countdown: { title: "Reprise", onGoing: false } };

    default:
      return state;
  }
}

function modeliseGame(tempGame, state) {
  tempGame.grid = Object.assign(new Grid(), tempGame.grid);
  let playerId;
  if (tempGame.players[0].id === socket.id) {
    playerId = 0;
  } else playerId = 1;
  if (state) {
    if (state.selectedCell) {
      if (state.selectedCell.type !== playerId) {
        tempGame.grid.possibleDups = state.game.grid.possibleDups;
        tempGame.grid.possibleJumps = state.game.grid.possibleJumps;
      }
    }
  }
  tempGame.players = modelisePlayers(tempGame.players);
  return Object.assign(new Game(), tempGame);
}

function modelisePlayers(newPlayers) {
  newPlayers[0] = Object.assign(new Player(), newPlayers[0]);
  newPlayers[1] = Object.assign(new Player(), newPlayers[1]);
  return newPlayers;
}

const getPossibleDups = (selectedCell, grid) => {
  return grid.filter(
    (cell) => Grid.adjacentTest(selectedCell, cell) && cell.type === Type.EMPTY
  );
};

const getPossibleJumps = (selectedCell, grid) => {
  return grid.filter(
    (cell) => Grid.jumpTest(selectedCell, cell) && cell.type === Type.EMPTY
  );
};
