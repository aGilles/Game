import * as actions from "../actions/ranking";

const defaultState = {
  dataDupz: [],
  dataFun: [],
};

export default function(state = defaultState, { type, payload }) {
  switch (type) {
    case actions.DUPZ_RANKING_FETCHED:
      return { ...state, dataDupz: payload };
    case actions.FUN_RANKING_FETCHED:
      return { ...state, dataFun: payload };
    default:
      return state;
  }
}
