export const UPDATE_NUMBERS = "UPDATE_NUMBERS";

export function updateNumbers(data) {
  return {
    type: UPDATE_NUMBERS,
    payload: data,
  };
}
