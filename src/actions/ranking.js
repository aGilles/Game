export const DUPZ_RANKING_FETCHED = "DUPZ_RANKING_FETCHED";
export const START_FETCH_DUPZ_RANKING = "START_FETCH_DUPZ_RANKING";
export const FUN_RANKING_FETCHED = "FUN_RANKING_FETCHED";
export const START_FETCH_FUN_RANKING = "START_FETCH_FUN_RANKING";

export function getDupzRanking() {
  return {
    type: START_FETCH_DUPZ_RANKING,
  };
}

export function dupzRankingFetched(data) {
  return {
    type: DUPZ_RANKING_FETCHED,
    payload: {
      data,
    },
  };
}

export function getFunRanking() {
  return {
    type: START_FETCH_FUN_RANKING,
  };
}

export function funRankingFetched(data) {
  return {
    type: FUN_RANKING_FETCHED,
    payload: {
      data,
    },
  };
}
