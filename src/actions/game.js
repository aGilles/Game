export const SELECT_CELL = "select_cell";
export const UPDATE_GAME = "update_game";
export const CREATE_GRID = "create_grid";
export const REGAIN_ENERGY = "regain_energy";
export const DISPLAY_ALERT = "display_alert";
export const NEW_MESSAGE_CHAT = "new_message_chat";
export const RESTART_GAME = "RESTART_GAME";
export const START_COUNTDOWN = "START_COUNTDOWN";
export const END_COUNTDOWN = "END_COUNTDOWN";

export function createGrid(game) {
  return {
    type: CREATE_GRID,
    payload: game,
  };
}
export function selectPiece(cell) {
  return {
    type: SELECT_CELL,
    payload: cell,
  };
}

export function updateGame(game) {
  return {
    type: UPDATE_GAME,
    payload: game,
  };
}

export function regainEnergy(id) {
  return {
    type: REGAIN_ENERGY,
    payload: id,
  };
}

export function displayAlert(alert) {
  return {
    type: DISPLAY_ALERT,
    payload: alert,
  };
}

export function restartGame() {
  return {
    type: RESTART_GAME,
  };
}

export function newMessage(className, content) {
  return {
    type: NEW_MESSAGE_CHAT,
    payload: {
      className,
      content,
    },
  };
}

export function startCountdown() {
  return {
    type: START_COUNTDOWN,
  };
}

export function endCountdown() {
  return {
    type: END_COUNTDOWN,
  };
}
