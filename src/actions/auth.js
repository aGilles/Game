export const AUTHENTICATE = "AUTHENTICATE";
export const AUTHENTICATE_STARTED = "AUTHENTICATE_STARTED";
export const AUTHENTICATE_SUCCEEDED = "AUTHENTICATE_SUCCEEDED";
export const AUTHENTICATE_FAILED = "AUTHENTICATE_FAILED";

export function authenticate(username, password, registration) {
  return {
    type: AUTHENTICATE,
    payload: {
      username,
      password,
      registration,
    },
  };
}

export function authenticateStarted() {
  return {
    type: AUTHENTICATE_STARTED,
  };
}

export function authenticateSucceeded(session) {
  return {
    type: AUTHENTICATE_SUCCEEDED,
    payload: {
      session,
    },
  };
}

export function authenticateFailed(error) {
  return {
    type: AUTHENTICATE_FAILED,
    payload: {
      error: error.message,
    },
  };
}
