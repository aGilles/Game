import { serverURL } from "../consts";

import { call, put, takeEvery } from "redux-saga/effects";

import * as actions from "../actions/ranking";

async function fetchRanking(url) {
  const options = {
    method: "GET",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  };
  const response = await fetch(
    serverURL + "src/controllers/users/ranking/" + url,
    options
  );
  const json = await response.json();
  if (json.error) {
    throw new Error(json.error);
  }
  return json;
}

function* performFetchDupzRanking() {
  try {
    const users = yield call(fetchRanking, "dupz");

    const data = users.map((user, index) => ({
      key: index + 1,
      rank: index + 1,
      name: user.name,
      rating: user.rating,
    }));
    yield put(actions.dupzRankingFetched(data));
  } catch (error) {
    console.log(error);
  }
}

function* performFetchFunRanking() {
  try {
    const users = yield call(fetchRanking, "fun");

    const data = users.map((user, index) => ({
      key: index + 1,
      rank: index + 1,
      name: user.name,
      rating: user.ratingFun,
    }));
    yield put(actions.funRankingFetched(data));
  } catch (error) {
    console.log(error);
  }
}

export default function*() {
  yield takeEvery(actions.START_FETCH_DUPZ_RANKING, performFetchDupzRanking);
  yield takeEvery(actions.START_FETCH_FUN_RANKING, performFetchFunRanking);
}
