import { call, put, takeEvery } from "redux-saga/effects";
import { push } from "connected-react-router";

import * as actions from "../actions/auth";
import { createSession } from "../services/SessionService";
import User from "../models/User";
import { Session } from "../models/Session";
import { serverURL } from "../consts";

async function login(username, password, registration) {
  const url = registration ? "add" : "login";
  const options = {
    method: "POST",
    body: JSON.stringify({
      name: username,
      password: password,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  };
  const response = await fetch(
    serverURL + "src/controllers/users/" + url,
    options
  );
  const json = await response.json();
  if (json.error) {
    throw new Error(json.error);
  }
  return json;
}

function* performAuthenticate({ payload }) {
  try {
    yield put(actions.authenticateStarted());

    const { username, password, registration } = payload;

    const json = yield call(login, username, password, registration);

    const user = new User(json.user.name, json.user._id);
    createSession(user, json.token);

    const session = new Session(user, json.token);
    yield put(actions.authenticateSucceeded(session));

    yield put(push("/game/lobby"));
  } catch (error) {
    yield put(actions.authenticateFailed(error));
  }
}

export default function*() {
  yield takeEvery(actions.AUTHENTICATE, performAuthenticate);
}
