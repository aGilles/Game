import { all } from "redux-saga/effects";

import authenticationSaga from "./authentication";

import rankingSaga from "./ranking";

export default function*() {
  yield all([authenticationSaga(), rankingSaga()]);
}
