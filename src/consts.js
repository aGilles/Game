export const serverURL =
  process.env.NODE_ENV !== "production"
    ? "http://localhost:4000/"
    : "https://api.dupz.cyrilchandelier.com/";
