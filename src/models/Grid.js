export default class Grid {
  grid = [];
  possibleDups = [];
  possibleJumps = [];

  constructor() {
    this.addPiece = this.addPiece.bind(this);
  }

  createGrid(rows, cols) {
    this.grid = [];
    let type = Type.EMPTY;
    let id = 0;
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        const coords = { i, j };
        let cell = { coords, type, id };
        this.grid.push(cell);
        id++;
      }
    }
  }

  addHole(id) {
    this.grid[id].type = Type.HOLE;
  }

  addPiece(id, type) {
    this.grid[id].type = type;
    this.players[type].points++;
  }

  static adjacentTest(cellFrom, cellTo) {
    if (
      Math.abs(cellFrom.coords.i - cellTo.coords.i) <= 1 &&
      Math.abs(cellFrom.coords.j - cellTo.coords.j) <= 1
    )
      return true;
    return false;
  }

  static jumpTest(cellFrom, cellTo) {
    const jumpRow =
      Math.abs(cellFrom.coords.i - cellTo.coords.i) === 2 &&
      cellFrom.coords.j === cellTo.coords.j;
    const jumpColumn =
      Math.abs(cellFrom.coords.j - cellTo.coords.j) === 2 &&
      cellFrom.coords.i === cellTo.coords.i;
    if (jumpRow || jumpColumn) return true;
    return false;
  }

  duplicatePiece(cellFrom, cellTo) {
    //set the type that's gonna eat
    let typeTo = cellFrom.type;
    //change type of empty cell
    this.grid[cellTo.id].type = typeTo;
    this.players[typeTo].points++;

    this.eatPieces(cellTo, typeTo);
  }

  jumpPiece(cellFrom, cellTo) {
    //set the type that's gonna eat
    let typeTo = cellFrom.type;
    //change type of empty cell
    this.grid[cellTo.id].type = typeTo;
    //change type of the cell that loses the piece with the jump
    this.grid[cellFrom.id].type = Type.EMPTY;

    this.eatPieces(cellTo, typeTo);
  }

  eatPieces(cellTo, typeTo) {
    // set the type that's gonna be eaten
    let typeToEat;
    if (typeTo === Type.PLAYER1) typeToEat = Type.PLAYER2;
    else typeToEat = Type.PLAYER1;

    //eat only the adjacents to the destination cell which contains the typeToEat
    this.grid = this.grid.map((cell) => {
      if (Grid.adjacentTest(cell, cellTo) && cell.type === typeToEat) {
        cell.type = typeTo;
        this.players[typeToEat].points--;
        this.players[typeTo].points++;
      }
      return cell;
    });
  }
}

export const Type = {
  EMPTY: "empty",
  PLAYER1: 0,
  PLAYER2: 1,
  HOLE: "hole",
};
