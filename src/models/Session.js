export class Session {
    constructor(user, token) {
        this.user = user;
        this.token = token;
    }
    socket;
    setSocket(socket) {
        this.socket = socket;
    }
}