export default class Player {
  name;
  number;
  points;
  energy;

  constructor(name, number) {
    this.name = name;
    this.number = number;
    this.points = 0;
    this.energy = 3;
  }

  incrementEnergy() {
    this.energy++;
  }

  decreaseEnergy(n) {
    this.energy -= n;
  }

  getName() {
    return this.name;
  }

  getNumber() {
    return this.number;
  }

  getPlayer() {
    return { name: this.name, number: this.number, points: this.points };
  }
}
