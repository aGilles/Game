import Grid, { Type } from "./Grid";

export default class Game {
  grid;
  players = [];
  endGame = false;
  lastPlayerPlayed;
  lastCell;
  constructor() {
    this.grid = new Grid();
  }

  getGame() {
    return {
      grid: this.grid,
      players: this.players,
    };
  }
  setGrid(grid) {
    this.grid = grid;
  }

  setEndGame() {
    this.endGame = true;
  }

  updatePlayers(players) {
    this.players = players;
  }

  endDetection() {
    if (this.players[0].points === 0 || this.players[1].points === 0) {
      return true;
    }
    if (!this.isThereAtLeastOneMove(0)) return true;
    if (!this.isThereAtLeastOneMove(1)) return true;

    return false;
  }

  isThereAtLeastOneMove(idPlayer) {
    let gridToSimulate = this.grid.grid;
    let end = false;

    gridToSimulate
      .filter((cell) => cell.type === this.players[idPlayer].number)
      .forEach((cell) => {
        gridToSimulate
          .filter(
            (cell2) =>
              Grid.adjacentTest(cell, cell2) || Grid.jumpTest(cell, cell2)
          )
          .forEach((cell3) => {
            if (cell3.type === Type.EMPTY) end = true;
          });
      });

    return end;
  }

  createMap() {
    const rand = Math.floor(Math.random() * 3);
    switch (rand) {
      case 0:
        this.createMap1();
        break;
      case 1:
        this.createMap2();
        break;
      case 2:
        this.createMap3();
        break;
      default:
        this.createMap3();
    }
  }

  createMap1() {
    this.grid.createGrid(4, 7);
    this.grid.addPiece(0, Type.PLAYER1);
    this.grid.addPiece(27, Type.PLAYER2);
    this.grid.addHole(18);
  }

  createMap2() {
    this.grid.createGrid(7, 7);
    this.grid.addPiece(0, Type.PLAYER1);
    this.grid.addPiece(48, Type.PLAYER2);
    this.grid.addHole(24);
  }

  createMap3() {
    this.grid.createGrid(6, 6);
    this.grid.addPiece(0, Type.PLAYER1);
    this.grid.addPiece(35, Type.PLAYER2);
    this.grid.addHole(7);
    this.grid.addHole(8);
    this.grid.addHole(9);
    this.grid.addHole(10);
    this.grid.addHole(13);
    this.grid.addHole(16);
    this.grid.addHole(19);
    this.grid.addHole(22);
    this.grid.addHole(28);
    this.grid.addHole(27);
    this.grid.addHole(26);
    this.grid.addHole(25);
  }
}

export const Turn = {
  PLAYER1: "player1",
  PLAYER2: "player2",
};
